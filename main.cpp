#include "dglk.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
using namespace std;

float t;

GLuint prog;
GLuint wvp_uniform_loc;
GLuint inv_res_uniform_loc;
GLuint time_uniform_loc;

const GLchar* vs_source = SHADER(410,
in vec3 pos;
uniform mat4 wvp;
void main() {
	gl_Position = vec4(pos,1.f)*wvp;
});

const GLchar* fs_source = SHADER(410,
uniform vec2 iResolution;
uniform float iGlobalTime;
out vec4 color;

float sdPlane( vec3 p )
{
	return p.y;
}

float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}


vec2 opU( vec2 d1, vec2 d2 )
{
	return (d1.x<d2.x) ? d1 : d2;
}

float SRamp3(float x, float k)
{
   float xp = clamp(-x * k + 0.5, 0.0, 1.0);
   
   float xp2 = xp * xp;
    
   return min(x, xp2 * (xp2 * 0.5 - xp) / k);
}

float smn(float a, float b, float k)
{
    return a + SRamp3(b - a, k);
}

vec2 map( in vec3 pos )
{
	float r = 3.f * cos(300.f*iGlobalTime);
 	vec2 res = vec2(
			smn(sdSphere(pos, 2.f), sdSphere(pos-vec3(r*cos(iGlobalTime*80.f), 0.f, r*sin(iGlobalTime*80.f)), 1.f), 1.f)
			
			, 1.f);
	return res;
}

vec2 castRay( in vec3 ro, in vec3 rd )
{
    float tmin = 1.0;
    float tmax = 20.0;
    
	float precis = 0.002;
    float t = tmin;
    float m = -1.0;
    for( int i=0; i<50; i++ )
    {
	    vec2 res = map( ro+rd*t );
        if( res.x<precis || t>tmax ) break;
        t += res.x;
	    m = res.y;
    }

    if( t>tmax ) m=-1.0;
    return vec2( t, m );
}

vec3 calcNormal( in vec3 pos )
{
	vec3 eps = vec3( 0.001, 0.0, 0.0 );
	vec3 nor = vec3(
	    map(pos+eps.xyy).x - map(pos-eps.xyy).x,
	    map(pos+eps.yxy).x - map(pos-eps.yxy).x,
	    map(pos+eps.yyx).x - map(pos-eps.yyx).x );
	return normalize(nor);
}


vec3 render( in vec3 ro, in vec3 rd )
{ 
    vec3 col = vec3(0.7, 0.9, 1.0) +rd.y*0.8;
    vec2 res = castRay(ro,rd);
    float t = res.x;
	float m = res.y;
    if( m>-0.5 )
    {
        col = vec3(t*.1);

    }

	return vec3( clamp(col,0.0,1.0) );
}

mat3 setCamera( in vec3 ro, in vec3 ta, float cr )
{
	vec3 cw = normalize(ta-ro);
	vec3 cp = vec3(sin(cr), cos(cr),0.0);
	vec3 cu = normalize( cross(cw,cp) );
	vec3 cv = normalize( cross(cu,cw) );
    return mat3( cu, cv, cw );
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 q = fragCoord.xy/iResolution.xy;
    vec2 p = -1.0+2.0*q;
	p.x *= iResolution.x/iResolution.y;
		 
	float time = 15.0 + iGlobalTime;

	// camera	
	vec3 ro = vec3( 0.f, 3.f, -6.f );
	vec3 ta = vec3( 0.f, 0.f, 0.f );
	
	// camera-to-world transformation
    mat3 ca = setCamera( ro, ta, 0.0 );
    
    // ray direction
	vec3 rd = ca * normalize( vec3(p.xy,2.0) );

    // render	
    vec3 col = render( ro, rd );

	col = pow( col, vec3(0.4545) );

    fragColor=vec4( col, 1.0 );
}

void main() {
	mainImage(color, gl_FragCoord.xy);
});

GLuint VBO, VAO, EBO;

void init() {
	glClearColor(0.3f, 0.3f, 0.4f, 1.f);
	t = 0.f;

	auto vs = load_shader(GL_VERTEX_SHADER, vs_source, strlen(vs_source));
	auto fs = load_shader(GL_FRAGMENT_SHADER, fs_source, strlen(fs_source));
	GLint success = 0;
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if(success == GL_FALSE) {
		GLint log_size = 0;
		glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &log_size);
		string log(log_size, ' ');
		glGetShaderInfoLog(fs, log_size, &log_size, (GLchar*)log.data());
		cout << "Compilation Log:" << endl << log << endl;
	}
	prog = glCreateProgram();
	glAttachShader(prog, vs);
	glAttachShader(prog, fs);
	glLinkProgram(prog);

	wvp_uniform_loc = glGetUniformLocation(prog, "wvp");
	inv_res_uniform_loc = glGetUniformLocation(prog, "iResolution");
	time_uniform_loc = glGetUniformLocation(prog, "iGlobalTime");


    	GLfloat vertices[] = {
    	    // Positions          
    	     1.0f,  1.0f, 0.0f,   
    	     1.0f, -1.0f, 0.0f,  
    	    -1.0f, -1.0f, 0.0f, 
    	    -1.0f,  1.0f, 0.0f,
    	};
    	GLuint indices[] = {  // Note that we start from 0!
    	    0, 1, 3, // First Triangle
    	    1, 2, 3  // Second Triangle
    	};

	glGenVertexArrays(1, &VAO);
    	glGenBuffers(1, &VBO);
    	glGenBuffers(1, &EBO);

    	glBindVertexArray(VAO);

    	glBindBuffer(GL_ARRAY_BUFFER, VBO);
    	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    	// Position attribute
    	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void render_loop() {
	t += 0.00016f;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(prog);
	
	auto mat = glm::mat4(1);
	glUniformMatrix4fv(wvp_uniform_loc, 1, GL_FALSE, &mat[0][0]);
	auto ir = glm::vec2(800.f, 600.f);
	glUniform2fv(inv_res_uniform_loc, 1, &ir[0]); 
	glUniform1fv(time_uniform_loc, 1, &t);

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void __std_terminate() { }

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow) {
	glcontext cx;
	init_gl(800, 600, &cx);
	init();
	loop_gl(&cx, &render_loop);
	quit_gl(&cx);
	ExitProcess(0);
}
